{{- define "base-dotnet.serviceaccount.tpl" -}}

{{- if .Values.serviceAccount.create }}
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "base-dotnet.serviceAccountName" . }}
  labels: {{- include "labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  annotations:
    {{- if .Values.serviceAccount.annotations }}
    {{- include "tplvalues.render" ( dict "value" .Values.serviceAccount.annotations "context" $) | nindent 4 }}
    {{- end }}
    {{- if .Values.commonAnnotations }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
{{- end }}


{{- end -}}
{{- define "base-dotnet.serviceaccount" -}}
{{- include "base-dotnet.util.merge" (append . "base-dotnet.serviceaccount.tpl") -}}
{{- end -}}
