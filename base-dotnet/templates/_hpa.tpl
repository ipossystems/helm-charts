{{- define "base-dotnet.hpa.tpl" -}}

{{- if .Values.autoscaling.enabled }}
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: {{ include "base-dotnet.fullname" . }}
  labels: {{- include "labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
spec:
  scaleTargetRef:
    apiVersion: {{ include "capabilities.deployment.apiVersion" . }}
    kind: Deployment
    name: {{ include "base-dotnet.fullname" . }}
  minReplicas: {{ .Values.autoscaling.minReplicas }}
  maxReplicas: {{ .Values.autoscaling.maxReplicas }}
  metrics:
    {{- if .Values.autoscaling.targetCPU }}
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: {{ .Values.autoscaling.targetCPU }}
    {{- end }}
    {{- if .Values.autoscaling.targetMemory }}
    - type: Resource
      resource:
        name: memory
        target:
          type: Utilization
          averageUtilization: {{ .Values.autoscaling.targetMemory }}
    {{- end }}
{{- end }}

{{- end -}}
{{- define "base-dotnet.hpa" -}}
{{- include "base-dotnet.util.merge" (append . "base-dotnet.hpa.tpl") -}}
{{- end -}}