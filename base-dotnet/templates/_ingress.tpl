{{- define "base-dotnet.ingress.tpl" -}}

{{- if .Values.ingress.enabled -}}
apiVersion: {{ include "capabilities.ingress.apiVersion" . }}
kind: Ingress
metadata:
  name: {{ include "base-dotnet.fullname" . }}
  labels: {{- include "labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  annotations:
    {{- if .Values.ingress.certManager }}
    kubernetes.io/tls-acme: "true"
    {{- end }}
    {{- if .Values.ingress.annotations }}
    {{- include "tplvalues.render" ( dict "value" .Values.ingress.annotations "context" $) | nindent 4 }}
    {{- end }}
    {{- if .Values.commonAnnotations }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
    {{- end }}
spec:
  rules:
    {{- if .Values.ingress.hostname }}
    - host: {{ .Values.ingress.hostname }}
      http:
        paths:
          - path: {{ .Values.ingress.path }}
            {{- if eq "true" (include "ingress.supportsPathType" .) }}
            pathType: {{ .Values.ingress.pathType }}
            {{- end }}
            backend: {{- include "ingress.backend" (dict "serviceName" (include "base-dotnet.fullname" .) "servicePort" "http" "context" $)  | nindent 14 }}
    {{- end }}
    {{- range .Values.ingress.extraHosts }}
    - host: {{ .name }}
      http:
        paths:
          - path: {{ default "/" .path }}
            backend:
              serviceName: {{ include "base-dotnet.fullname" $ }}
              servicePort: http
    {{- end }}
  {{- if or .Values.ingress.tls .Values.ingress.extraTls .Values.ingress.hosts }}
  tls:
    {{- if .Values.ingress.tls }}
    - hosts:
        - {{ .Values.ingress.hostname }}
      secretName: {{ printf "%s-tls" .Values.ingress.hostname }}
    {{- end }}
    {{- if .Values.ingress.extraTls }}
    {{- toYaml .Values.ingress.extraTls | nindent 4 }}
    {{- end }}
  {{- end }}
{{- end }}

{{- end -}}
{{- define "base-dotnet.ingress" -}}
{{- include "base-dotnet.util.merge" (append . "base-dotnet.ingress.tpl") -}}
{{- end -}}
