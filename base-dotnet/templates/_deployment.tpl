{{- define "base-dotnet.deployment.tpl" -}}

apiVersion: {{ include "capabilities.deployment.apiVersion" . }}
kind: Deployment
metadata:
  name: {{ include "base-dotnet.fullname" . }}
  labels: {{- include "labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels: {{- include "labels.matchLabels" . | nindent 6 }}
  strategy:
    type: {{ .Values.strategyType }}
    {{- if (eq "Recreate" .Values.strategyType) }}
    rollingUpdate: null
    {{- end }}
  template:
    metadata:
      {{- if .Values.podAnnotations }}
      annotations: {{- include "tplvalues.render" (dict "value" .Values.podAnnotations "context" $) | nindent 8 }}
      {{- end }}
      labels: {{- include "labels.standard" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "base-dotnet.serviceAccountName" . }}
      {{- if .Values.affinity }}
      affinity: {{- include "tplvalues.render" ( dict "value" .Values.affinity "context" $) | nindent 8 }}
      {{- else }}
      affinity:
        podAffinity: {{- include "affinities.pods" (dict "type" .Values.podAffinityPreset "context" $) | nindent 10 }}
        podAntiAffinity: {{- include "affinities.pods" (dict "type" .Values.podAntiAffinityPreset "context" $) | nindent 10 }}
        nodeAffinity: {{- include "affinities.nodes" (dict "type" .Values.nodeAffinityPreset.type "key" .Values.nodeAffinityPreset.key "values" .Values.nodeAffinityPreset.values) | nindent 10 }}
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector: {{- include "tplvalues.render" ( dict "value" .Values.nodeSelector "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "tplvalues.render" (dict "value" .Values.tolerations "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName | quote }}
      {{- end }}
      {{- if .Values.podSecurityContext.enabled }}
      securityContext:
        fsGroup: {{ .Values.podSecurityContext.fsGroup }}
        {{- if .Values.podSecurityContext.sysctls }}
        sysctls:
          {{- toYaml .Values.podSecurityContext.sysctls | nindent 10 }}
        {{- end }}
      {{- end }}
      {{- if .Values.initContainers }}
      initContainers:
        {{- include "tplvalues.render" (dict "value" .Values.initContainers "context" $) | nindent 8 }}
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy | quote }}
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext:
            runAsUser: {{ .Values.containerSecurityContext.runAsUser }}
          {{- end }}
          env:
            - name: ASPNETCORE_URLS
              value: {{ .Values.bindURLs | quote }}
            {{- range $key, $value := .Values.extraEnvVars }}
            - name: {{ $key }}
              value: "{{ $value }}"
            {{- end }}
          envFrom:
            {{- if .Values.extraEnvVarsCM }}
            - configMapRef:
                name: {{ tpl .Values.extraEnvVarsCM . | quote }}
            {{- end }}
            {{- if .Values.extraEnvVarsSecret }}
            - secretRef:
                name: {{ tpl .Values.extraEnvVarsSecret . | quote }}
            {{- end }}  
          {{- if .Values.lifecycleHooks }}
          lifecycle: {{- include "tplvalues.render" (dict "value" .Values.lifecycleHooks "context" $) | nindent 12 }}
          {{- end }}
          ports:
            - name: http
              containerPort: {{ .Values.containerPort }}
          {{- if .Values.livenessProbe.enabled }}
          livenessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .Values.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.livenessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.livenessProbe.successThreshold }}
            failureThreshold: {{ .Values.livenessProbe.failureThreshold }}
          {{- else if .Values.customLivenessProbe }}
          livenessProbe: {{- include "tplvalues.render" (dict "value" .Values.customLivenessProbe "context" $) | nindent 12 }}
          {{- end }}
          {{- if .Values.readinessProbe.enabled }}
          readinessProbe:
            tcpSocket:
              port: http
            initialDelaySeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.readinessProbe.failureThreshold }}
          {{- else if .Values.customReadinessProbe }}
          readinessProbe: {{- include "tplvalues.render" (dict "value" .Values.customReadinessProbe "context" $) | nindent 12 }}
          {{- end }}
          {{- if .Values.resources }}
          resources: {{- toYaml .Values.resources | nindent 12 }}
          {{- end }}
        {{- if .Values.sidecars }}
        {{- include "tplvalues.render" ( dict "value" .Values.sidecars "context" $) | nindent 8 }}
        {{- end }}

{{- end -}}
{{- define "base-dotnet.deployment" -}}
{{- include "base-dotnet.util.merge" (append . "base-dotnet.deployment.tpl") -}}
{{- end -}}
